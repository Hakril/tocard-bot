import re
from msghandler import MsgHandler

class PingMsgHandler(MsgHandler):
    name = "ping"
    def __init__(self):
        super(PingMsgHandler, self).__init__()
        self.regexp = re.compile("PING :(?P<serveur>.*)")

    def handle(self, match):
        serv = match.group("serveur")
        self.send("PONG :{0}".format(serv))
