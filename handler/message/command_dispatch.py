import re

from msghandler import PrivMsgHandler
from handler.command.command import CmdHandler 
from dispatcher import Dispatcher, UnloadImpossibleException, UnloadedHandlerException, InvalidHandlerException 

class CmdMsgHandler(Dispatcher, PrivMsgHandler):
    name = "cmd_dispatch"
    noregister = True
    def __init__(self):
        super(CmdMsgHandler, self).__init__()
        self.regexp = re.compile( ":(?P<nick>[^!]*)!\S* PRIVMSG (?P<dest>\S*) :(?P<msg>.*)")
        self.command_template = re.compile("!(?P<cmd>\S*) ?(?P<args>.*)")

    # PrivMsgHandler
    def handle(self, match):
        nick = match.group('nick')
        dest = match.group('dest')
        msg = match.group('msg')
        cmd_match = self.command_template.match(match.group('msg'))

        if not dest.startswith("#"):
            dest = nick

        if cmd_match:
            cmd = cmd_match.group('cmd').lower()
            args = cmd_match.group('args')
            self.dispatch(nick, dest, cmd, args)

    def notice_unregistration(self, quitting):
        if not quitting:
            raise UnloadImpossibleException(self.name)
        self.quit() 

    # Dispatcher
    def is_valid_handler(self, handler):
        return isinstance(handler, CmdHandler)

    def can_access(self, nick, dest, cmd, args):
        return True

    def dispatch(self, nick, dest, cmd, args):
            for handler in self.handlers.values():
                if handler.match(cmd):
                    if not self.can_access(nick, dest, cmd, args):
                        self.send("You don't have access to that command !", dest)
                        return
                    def response_send(msg):
                        self.send(msg, dest)
                    handler.set_send(response_send)
                    handler.handle(nick, dest, cmd, args)
