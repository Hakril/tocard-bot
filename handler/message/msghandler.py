import re
from handler.irchandler import IrcHandler


class MsgHandler(IrcHandler):
    pass


class PrivMsgHandler(MsgHandler):
    Priv_Regexp = ":(?P<nick>[^!]*)!\S* PRIVMSG (?P<dest>\S*) :"
    def set_send(self, send_method):
        def send_privmsg(msg, dest):
            send_method("PRIVMSG {0} :{1}\r\n".format(dest, msg))
        self.send = send_privmsg
