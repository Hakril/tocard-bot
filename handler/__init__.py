#from message import *
#from command import *


import message
import command
from irchandler import IrcHandler
from message.msghandler import MsgHandler

def get(module, attr):
    return module.__getattribute__(attr)


def load_handlers():
    for mod_name in message.__all__:
        msg_handlers.update(load_submodule(message, mod_name, classfilter=MsgHandler))

    for mod_name in command.__all__:
        cmd_handlers.update(load_submodule(command, mod_name, classfilter=IrcHandler))

def load_submodule(surmod, mod_name, classfilter=None):
    res = {}
    module = get(surmod, mod_name)
    for h_name in dir(module):
        h = get(module, h_name)
        if type(h) == type(type) and classfilter and issubclass(h, classfilter):
            if hasattr(h, "name") and h.name:
                res.update({h.name : h})
    return res



def reload_command(mod_name):
    global command
    reload (command)
    if mod_name not in dir(command):
        return False

    reload(get(command, mod_name))
    cmd_handlers.update(load_submodule(command, mod_name, classfilter=IrcHandler))
    return True

def reload_message(mod_name):
    global message
    reload (message)
    if mod_name not in dir(message):
        return False

    reload(get(message, mod_name))
    msg_handlers.update(load_submodule(message, mod_name, classfilter=MsgHandler))
    return True


#On import
msg_handlers = {}
cmd_handlers = {}
load_handlers()

__all__ = ['cmd_handlers', 'msg_handlers', 'reload_command', 'reload_message']
