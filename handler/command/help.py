import re
from command import CmdHandler

class HelpCmdHandler(CmdHandler):
  """Help command. Give help on the loaded command of the bot."""

  name = "help"
  access = {"help" : set(["all"])}

  def __init__(self):
      super(HelpCmdHandler, self).__init__()
      self.regexp = re.compile(r"help$")

  def notice_registration(self, cmd_dispatcher):
      self.cmd_handlers = cmd_dispatcher.handlers;

  def handle(self, nick, dest, cmd, arg):
      args = arg.split()
      if (len(args) == 1) and args[0] in self.cmd_handlers:
          cmdhandler = self.cmd_handlers[args[0]];
          if (cmdhandler.__doc__ is not None):
              self.send(cmdhandler.__doc__)
              return
      msg = "Commands are accessible with !CMD. You can also get more help "
      msg += "with !help CMD. The list of available commands is: "
      msg += ", ".join([c.name for c in self.cmd_handlers.values() if c.__doc__
                        is not None])
      msg += "."
      self.send(msg)
