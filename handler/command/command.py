import re
from handler.irchandler import IrcHandler

class NotImplementedCmdHandler(Exception):
    pass

class CmdHandler(IrcHandler):

    def handle(self, nick, dest, cmd, arg):
        raise NotImplementedCmdHandler("command handle not implemented")

    def notice_registration(self, cmd_dispatcher):
        self.self_unload = lambda : cmd_dispatcher.unregister(self.name)

    def notice_unregistration(self, quitting):
        self.self_unload = lambda : None



class TestCmdHandler(CmdHandler):
    name = "test"

    def __init__(self):
        super(TestCmdHandler, self).__init__()
        self.regexp = re.compile(r"test$")

    def handle(self,nick, dest, cmd, arg):
        self.send("TEST WITH ARG:{0}".format(arg))


class QuitCmdHandler(CmdHandler):
    name = "bye"

    def __init__(self):
        super(EvalCmdHandler, self).__init__()
        self.regexp = re.compile(r"({[Q]uit|[dD]eco|[bB]ye)")

    def handle(self, nick, dest, cmd, arg):
        self.send("Bye !")
        exit()
