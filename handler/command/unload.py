import re
from command import CmdHandler
from dispatcher import UnloadImpossibleException, UnloadedHandlerException

class UnloadCmdHandler(CmdHandler):
    """Unload a loaded command: !unload <cmd>"""
    name = "unload"
    access = {name : set(['admin'])}

    def __init__(self):
        super(UnloadCmdHandler, self).__init__()
        self.regexp = re.compile(r"unload$")

    def notice_registration(self, cmd_dispatcher):
        self.cmd_dispatcher = cmd_dispatcher

    def notice_unregistration(self, quitting):
        if not quitting:
            raise UnloadImpossibleException(self.name)

    def handle(self, nick, dest, cmd, arg):
        args = arg.split()
        if not args:
            self.send("This command needs an argument.")
            return
        try:
            self.cmd_dispatcher.unregister(args[0])
            self.send("Command <{0}> succesfully unloaded!".format(args[0]))
        except UnloadImpossibleException as e:
            self.send(e)
        except UnloadedHandlerException as e:
            self.send(e)
