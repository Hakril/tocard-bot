import re
from command import CmdHandler

class PdbCmdHandler(CmdHandler):
    """This Command allow to run a pdb on the bot"""
    name = "pdb"
    access = {name : set(['admin'])}

    def __init__(self):
        super(PdbCmdHandler, self).__init__()
        self.regexp = re.compile(r"pdb$")

    def notice_registration(self, cmd_dispatcher):
        super(PdbCmdHandler, self).notice_registration(cmd_dispatcher)
        self.dispatcher = cmd_dispatcher


    def handle(self, nick, dest, cmd, arg):
        self.send("Launch Pdb")
        import pdb; pdb.set_trace()
