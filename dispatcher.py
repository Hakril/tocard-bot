from handler.irchandler import IrcHandler

class InvalidHandlerException(Exception):
    def __init__(self, handler):
        self.handler = handler

    def __str__(self):
        return "Invalide Handler {0} for dispatcher".format(self.handler)


class UnloadImpossibleException(Exception):
    def __init__(self, cmd):
        self.cmd = cmd

    def __str__(self):
        return "Command <{0}> couldn't be unloaded!".format(self.cmd)


class UnloadedHandlerException(Exception):
    def __init__(self, cmd):
        self.cmd = cmd

    def __str__(self):
        return "Command <{0}> isn't loaded!".format(self.cmd)


class Dispatcher(object):
    def __init__(self):
        super(Dispatcher, self).__init__()
        self.handlers = {}
        pass

    def is_valid_handler(self, handler):
        return isinstance(handler, IrcHandler)

    def register(self,handler):
        if not self.is_valid_handler(handler):
            raise InvalidHandler(handler)

        self.handlers[handler.name] = handler
        handler.notice_registration(self)

    def quit(self):
        for handler in self.handlers.values():
                handler.notice_unregistration(True)
        self.handlers.clear()

    def unregister(self,handlername):
        """
            Unload a module, this function can raise:
                UnloadImpossibleException
                UnloadedHandlerException
        """
        if handlername not in self.handlers:
            raise UnloadedHandlerException(handlername)

        self.handlers[handlername].notice_unregistration(False)
        del self.handlers[handlername]

    def can_access(self, msg):
        return True

    def dispatch(self, msg):
        for handler in self.handlers.values():
            match = handler.match(msg)
            if match and self.can_access(msg):
                handler.handle(match)
